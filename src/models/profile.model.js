const mongoose = require("mongoose");
const profile = new mongoose.Schema({
    ruc: { type: String },
    razon_social: { type: String },
    estado: { type: String },
    direccion: { type: String },
    ubigeo: { type: String },
    departamento: { type: String },
    provincia: { type: String },
    distrito: { type: String },
})
let ProfileModel  = mongoose.model("profile", profile);
module.exports = {
    ProfileModel
}