const express = require("express");
const path = require("path");
const app = express();
const initBD = require("./config/init_db");
app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use("/api-docs", require("./docs/api.doc"))
app.use("/api",require("./routes/profile.router"))
app.use(express.static(path.join(__dirname, '../public')));
initBD(app.listen(process.env.PORT || 7000, () => {
        console.log("app is running")
    }))