const mongoose = require("mongoose");
const path = require("path")
const dotenv = require("dotenv")
dotenv.config({path: path.join(__dirname, "../../.env")})
module.exports = (app) => {
    mongoose.connect(process.env.MONGODB_PROD || process.env.MONGODB_LAB, (db) => {
        console.log("mongodb is connect")
    })
}