
const { ProfileModel } = require("../models/profile.model")
async function searchByRuc(ruc) {
    let response = await ProfileModel.findOne({ruc})
    return response;
}
async function saveProfile(data) {
    let profileModel = new ProfileModel({
        "ruc": data?.ruc,
        "razon_social": data?.razon_social,
        "estado": data?.estado,
        "direccion": data?.direccion,
        "ubigeo": data?.ubigeo,
        "departamento": data?.departamento,
        "provincia": data?.provincia,
        "distrito": data?.distrito,
    })
    let response = await profileModel.save().then();
    return response;
}

module.exports = {
    searchByRuc,
    saveProfile
}