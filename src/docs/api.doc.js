const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUiExpress = require("swagger-ui-express");
const app = require("express").Router();
const Options = {
    swaggerDefinition: {
        info: {
            title: "Prueba Tecnica",
            descripcion: "Para el puesto de desarrollador Back-end"
        },
        servers: ["http://localhost:7000"]
    },
    apis:["./src/*.js"]
}
const swaggerDocs = swaggerJsDoc(Options);
app.use("/", swaggerUiExpress.serve, swaggerUiExpress.setup(swaggerDocs))
module.exports = app