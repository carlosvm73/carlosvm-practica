const queryRuc = require("../helpers/ruc.helper")
const { searchByRuc, saveProfile } = require("../database/profile.db")
async function getProfile(req, res, next) {
    try {
        let exit = await searchByRuc(req?.body?.ruc)
        if(!exit) {
            let data = await queryRuc(req?.body?.tipo, req?.body?.ruc)
            let result = await saveProfile(data);
            return res.status(200).send(result)
        } else {
            return res.status(200).send(exit)
        }
    } catch (error) {
        return res.status(500).send(error?.message)
    }
}
module.exports = {
    getProfile
}