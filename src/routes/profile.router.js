const { getProfile } = require("../controllers/profile.controller")
const route = require("express").Router();
/**
 * @openapi
 * /api/profile:
 *  post:
 *    description: Obtienes los datos personales de una persona a traves de su documento de identidad
 *    responses:
 *      '200':
 *          descripcion: La respuesta ha sido satisfactoria
 */
route.post("/profile", getProfile)
module.exports = route;