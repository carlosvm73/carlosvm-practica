const request =  require("request");
/**
 * 
 * @param {Number | String} type  Recibe como parametro el tipo, que es el tipo del documento.
 * @param {Number | String} doc Recibe como paramentro el numero de documento, que es el numero de identificacion de la persona natural o juridica.
 * @returns Retorna un ojecto con los datos de perfil del numero del documento ingresado.
 */
module.exports = async (type, doc) => {
    return new Promise((resolve, reject) => {
        try {
            const api = `http://wsruc.com/Ruc2WS_JSON.php?tipo=${type}&ruc=${doc}&token=${process.env.KEY_SECRET_API}`;
            request(api, (err, response, body) => {
                if(err) {
                    throw Error("El servicio esta temporalmene fuera de servicio, Intentar mas tarde!")
                }
                switch(response.statusCode) {
                    case 200:
                        return resolve(JSON.parse(body));
                    case 404:
                        throw Error("Al parecer los datos ingresados no pertenece a ninguna persona, Intentalo nuevamente!")
                    default:
                        throw Error("El servicio esta temporalmene fuera de servicio, Intentar mas tarde!")
                }
            });
        } catch (error) {
            return reject(error)
        }
    })
}