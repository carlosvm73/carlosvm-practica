async function doRequest() {
  document.getElementById("container").setAttribute("class", "isload");
  var tipo = document.getElementById("tipo").value;
  var ruc = document.getElementById("ruc").value;
  let result = await fetch("http://localhost:7000/api/profile", {
    method: "POST",
    headers: { "content-type": "application/json" },
    body: JSON.stringify({ tipo, ruc }),
  }).then((chuck) => chuck.json());
  if (result) {
    document.getElementById("_ruc").innerHTML = result.ruc;
    document.getElementById("razon_social").innerHTML = result.razon_social;
    document.getElementById("estado").innerHTML = result.estado;
    document.getElementById("direccion").innerHTML = result.direccion;
    document.getElementById("ubigeo").innerHTML = result.ubigeo;
    document.getElementById("departamento").innerHTML = result.departamento;
    document.getElementById("provincia").innerHTML = result.provincia;
    document.getElementById("distrito").innerHTML = result.distrito;
    setTimeout(() => {
      document.getElementById("container").setAttribute("class", "");
    }, 1000);
  } else {
    document
      .getElementById("container")
      .setAttribute("data-message", "hubo un error, intentar mas tarde");
    setTimeout(() => {
      document.getElementById("container").setAttribute("class", "");
      document
        .getElementById("container")
        .setAttribute("data-message", "Cargando ...");
    }, 3000);
  }
}
window.addEventListener("load", () => {
  initialData();
  document
    .getElementById("clear")
    .addEventListener("click", () => initialData());
  function initialData() {
    // document.getElementById("tipo").value = ""
    // document.getElementById("ruc").value = ""
    document.getElementById("_ruc").innerHTML = "*****************";
    document.getElementById("razon_social").innerHTML = "*****************";
    document.getElementById("estado").innerHTML = "*****************";
    document.getElementById("direccion").innerHTML = "*****************";
    document.getElementById("ubigeo").innerHTML = "*****************";
    document.getElementById("departamento").innerHTML = "*****************";
    document.getElementById("provincia").innerHTML = "*****************";
    document.getElementById("distrito").innerHTML = "*****************";
  }
});
