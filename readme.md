# APP carlosvm-practica

Este proyecto es una demo para obtener los datos personales de una persona de nacionalidad Peruana a traves de su documento de identidad.

## Development Server

Para ejecutar el proyecto en modo developer `npm run server`. El cual tendra una salida al navegador `http://localhost:7000`.

## Page

En el navegador http://7fd0-179-6-101-185.ngrok.io
    - documentacion /api-docs
    - endpoints /api
                /api/profile                            POST
